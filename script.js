"use strict";

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let list = '';

function createList (array, parent = document.body) {
    array.forEach(item => list += `<li>${item}</li>`);
    parent.insertAdjacentHTML("beforeend", list);
}

createList (arr);